/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Termino;

/**
 *
 * @author gabriel
 */
public class Polinomio {
    private Termino[] termino;
    private int i=0;

    public Polinomio() {
    i=0;
    }
    
    
    public Polinomio(int n) {
        this.termino = new Termino[n];
    }
    
    public void addTermino(Termino x){
        if (this.getCapacidad()>= this.termino.length) {
            throw new RuntimeException("Lleno");
        }
        this.termino[i]=x;
        i++;
    
    }
    public String getDerivar() {
        if (this.getCapacidad() <= 0) {
            throw new RuntimeException("No hay ecuacion para derivar Bobo");
        }
        String msg = "Derivada -> ";
        for (int j = 0; j < this.getCapacidad(); j++) {
            float base, expo;
            base = (this.termino[j].getCoeficiente()* this.termino[j].getExponente());
            expo = (this.termino[j].getExponente() - 1);
            if (expo != 0 && this.termino[j].getExponente() != 0) {
                if (base >= 0) {
                    if (j != 0) {
                        msg += " + ";
                    }
                    msg += base + " " + this.termino[j].getLiteral() + "^" + expo + " ";
                } else {
                    msg += base + " " + this.termino[j].getLiteral() + "^" + expo + " ";
                }
            } else {
                if (base >= 0) {
                    msg += " + " + base + " ";
                } else {
                    msg += base + " ";
                }
            }
        }
        return msg;
    }
    
    
    
    @Override
    public String toString() {
        String msg="";
        for (int j = 0; j < this.getCapacidad(); j++) {
            msg+=termino[j].toString()+" ";
        }
        return msg;
    }
    
    public int getCapacidad(){
    return i;
    }

    public Termino[] getTermino() {
        return termino;
    }

    public void setTermino(Termino[] termino) {
        this.termino = termino;
    }
    
    
}
