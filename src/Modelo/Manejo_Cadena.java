/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author gabriel
 */
public class Manejo_Cadena {

    public char[] getCopiar(char s1[], char s2[]) {
        s2 = new char[s1.length];
        for (int i = 0; i < s1.length; i++) {
            s2[i] = s1[i];
        }
        return s2;
    }

    public char[] addFinal(char s1[], char s2[]) {
        char[] s3 = new char[s1.length + s2.length];
        for (int i = 0; i < s1.length; i++) {
            s3[i] = s1[i];

        }
        for (int i = 0; i < s2.length; i++) {
            s3[s1.length + i] = s2[i];
        }

        return s3;
    }

    public int getCantidadFrases(String x) {
        int i = 1;
        x = x.trim();
        for (int j = 0; j < x.length(); j++) {
            if (x.charAt(j) == ' ') {
                i++;
            }
        }
        return i;
    }

    public String imprimirLineasDiferentes(String x) {
        String msg = "";
        int i = 0;
        for (int j = 0; j < x.length(); j++) {
            if (x.charAt(j) != ' ') {
                msg += x.charAt(j);
            } else {
                msg += "\n";
            }

        }

        return msg;
    }

    public boolean esPalindroma(String x) {

        for (int i = 0; i < x.length(); i++) {
            if (x.charAt(i) != x.charAt(x.length() - i - 1)) {
                return false;
            }

        }
        return true;
    }

    private char ConvertidorMinusAMayus(char x) {
        switch (x) {
            case 'a':
                return 'A';
            case 'b':
                return 'B';
            case 'c':
                return 'C';
            case 'd':
                return 'D';
            case 'e':
                return 'E';
            case 'f':
                return 'F';
            case 'g':
                return 'G';
            case 'h':
                return 'H';
            case 'i':
                return 'I';
            case 'j':
                return 'J';
            case 'k':
                return 'K';
            case 'l':
                return 'L';
            case 'm':
                return 'M';
            case 'n':
                return 'N';
            case 'o':
                return 'O';
            case 'p':
                return 'P';
            case 'q':
                return 'Q';
            case 'r':
                return 'R';
            case 's':
                return 'S';
            case 't':
                return 'T';
            case 'u':
                return 'U';
            case 'v':
                return 'V';
            case 'w':
                return 'W';
            case 'x':
                return 'X';
            case 'y':
                return 'Y';
            case 'z':
                return 'Z';
            case 'á':
                return 'Á';
            case 'é':
                return 'É';
            case 'í':
                return 'Í';
            case 'ó':
                return 'Ó';
            case 'ú':
                return 'Ú';

            default:
                return x;

        }

    }

    public String getMayusculas(String x) {
        char[] y = new char[x.length()];
        String msg = "";
        for (int i = 0; i < x.length(); i++) {
            y[i] = this.ConvertidorMinusAMayus(x.charAt(i));
            msg += y[i];
        }
        return msg;
    }

    private char ConvertidorMayusAMinus(char x) {
        switch (x) {
            case 'A':
                return 'a';
            case 'B':
                return 'b';
            case 'C':
                return 'c';
            case 'D':
                return 'd';
            case 'E':
                return 'e';
            case 'F':
                return 'f';
            case 'G':
                return 'g';
            case 'H':
                return 'h';
            case 'I':
                return 'i';
            case 'J':
                return 'j';
            case 'K':
                return 'k';
            case 'L':
                return 'l';
            case 'M':
                return 'm';
            case 'N':
                return 'n';
            case 'O':
                return 'o';
            case 'P':
                return 'p';
            case 'Q':
                return 'q';
            case 'R':
                return 'r';
            case 'S':
                return 's';
            case 'T':
                return 't';
            case 'U':
                return 'u';
            case 'V':
                return 'v';
            case 'W':
                return 'w';
            case 'X':
                return 'x';
            case 'Y':
                return 'y';
            case 'Z':
                return 'z';
            case 'Á':
                return 'á';
            case 'É':
                return 'é';
            case 'Í':
                return 'í';
            case 'Ó':
                return 'ó';
            case 'Ú':
                return 'ú';

            default:
                return x;

        }

    }

    public String getMinusculas(String x) {
        char[] y = new char[x.length()];
        String msg = "";
        for (int i = 0; i < x.length(); i++) {
            y[i] = this.ConvertidorMayusAMinus(x.charAt(i));
            msg += y[i];
        }
        return msg;
    }

    public int getCuantasVecesEsta(String x, String y) {
        int aux2 = 0;
        for (int i = 0; i <= x.length() - y.length(); i++) {
            int auxCont = 0;

            for (int j = 0; j < y.length(); j++) {
                if (x.charAt(i + j) == y.charAt(j)) {
                    auxCont++;
                }
                if (auxCont == y.length()) {
                    aux2++;
                    auxCont = 0;
                }
            }

        }
        return aux2;
    }

}
