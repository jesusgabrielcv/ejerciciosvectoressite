/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author gabriel
 */
public class Termino {
    
    private float coeficiente;
    private char literal;
    private int exponente;
    
    public Termino() {
    }

    public Termino(float coeficiente, char literal, int exponente) {
        
        this.coeficiente = coeficiente;
        this.literal = literal;
        this.exponente = exponente;
    }

   

    public float getCoeficiente() {
        return coeficiente;
    }

    public void setCoeficiente(float coeficiente) {
        this.coeficiente = coeficiente;
    }

    public char getLiteral() {
        return literal;
    }

    public void setLiteral(char literal) {
        this.literal = literal;
    }

    public int getExponente() {
        return exponente;
    }

    public void setExponente(int exponente) {
        this.exponente = exponente;
    }

       public Termino getSumar(Termino t2){
        if(this.getExponente()==t2.getExponente() && this.getLiteral()==t2.getLiteral()){
            return new Termino(this.coeficiente+t2.coeficiente,this.literal,this.exponente);
        }
        return null;
    }

      @Override
    public String toString() {
        String msg = "";
        if (this.getCoeficiente()> 0) {
            msg += " + ";
        }
        msg += this.getCoeficiente()+ "" + this.getLiteral() + "^" + this.getExponente();
        return msg;
    }
    
}
